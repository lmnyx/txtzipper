import os, sys
import time
import zipfile
from argparse import ArgumentParser
import shutil

parser = ArgumentParser()
parser.add_argument("-d", "--dir", dest="directory",
                    help="start directory", metavar="DIR")

args = parser.parse_args()
print("TxtZipper 2\n")
if(args.directory == None):
	print("[Error]: Provide argument!\n[Example]: py txtzipper.py -d D:/dir_with_txts")
	exit(1)

if(os.path.isfile("output.zip")):
	print("[Warning]: Removing output.zip...")
	os.remove("output.zip")
else:
	print("[OK]: output.zip doesn't exists. Everything is ok!")
total = 0
with zipfile.ZipFile('output.zip', 'w') as txtzip:
    for root, subdirs, files in os.walk(args.directory):
    	for file in files:
    		if(file.endswith(".txt")):
    			print("[OK]: " + root + "\\" + file + " meets the agreement.")
    			total += 1
    			if(root != os.path.dirname(os.path.realpath(__file__))):
    				shutil.copyfile(root + "\\" + file, file)
    			txtzip.write(file)
    			os.remove(file)


print("Zipped " + str(total) + " txt files in total.")